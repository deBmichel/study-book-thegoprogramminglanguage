/*
	Title?:
	An unexpected encounter with the power of Golang interfaces.

	The old code had the objective : collect example code from
	the Alan A. Donovan open repo in a faster way.

	But for work reasons I had to do a development to download
	urls and write downloaded urls to files, the core of the
	project was written in python, rust, ruby, Golang, c++, node
	and other tecnologies, and the requeriment was designed to
	be incorporated in the Golang section, So I thought I'll use
	the base that I wrotte to study the Golang book, because is
	similar: I download the code of the Alan A. Donovan repo and
	writte the downloaded code to a file using Golang.

	Yes Yes: I am reading a book about Golang in the work.

	And here I found the Golang interfaces and a thousand questions.

	Trick read the phases in Order. Total Phases = 4

	Phase 1 : To start :
	Phase 2 : To Think :
	Phase 3: this is like magic for Michel:
	Phase 4 : Investigate again : IT is a great problem for me ...
*/
package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
)

var ErrFileExists = errors.New("the file really exists")

func main() {
	// Phase 1 : To start : I like to write short and have played with
	// Golang for some time, So I thought: when I create the file if I
	// have no errors or complications I will return an io.Writer to
	// write, with the intention of use the file as fast as possible
	// and exactly with io.Copy :) I like io.Copy to delete fmt or log 3:).
	// But honestly I didn't know how to do the pass from File to io.Writer,
	// I had to search in: Baidu, yandex, duck duck go and some in google,
	// but the most important : https://golang.org/src/io/io.go?s=4030:4091#L86
	// is important review under the wood cowboys. :) Now a gopher has some
	// Ideas 3:)
	if writer, err := createFile(os.Args[2]); err == nil {
		url := os.Args[1]
		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}

		_, err = io.Copy(writer, resp.Body)
		resp.Body.Close()

		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: writting 1%s: %v\n", url, err)
			os.Exit(1)
		}

		endstr := "\n// I (Michel MS) found this code in the source :\n// " + os.Args[1] + "\n"
		_, err = io.Copy(writer, bytes.NewBufferString(endstr))
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: writting 2%s: %v\n", url, err)
			os.Exit(1)
		}

		// read this after Phase3 : #)
		// ###### ¿    writer.Close()  ? // ##### This line fails, here:
		// I though : MICHEL YOU have to close the writer to avoid leak
		// resources, the requeriment is designed to go to a big ammount
		// of files and urls, you will have to ensure the close of the
		// writer.
		// but my code fails, cause io.Writter obviously does not have
		// a .close method ... I personally reviewed the code :
		// https://golang.org/src/io/io.go?s=4030:4091#L86
		// But what happens with the openFile , ¿is not openFile? ¿is openFile
		// closed? ¿¿¿¿¿ am i leaking resources ????
	} else {
		fmt.Fprintf(os.Stderr, "CreateFile has failed with error: %v\n", err)
	}
}

func createFile(filepath string) (io.Writer /*The magic is here???*/, error) {
	// Phase 2 : To Think : To create this function I honestly had to search in
	// Baidu,( A few months ago I started to lose faith in google and stackoverflow)
	// and in baidú I found this source : https://www.it1352.com/809050.html
	// if you review the comment //这会导致内存不足, in the source somebody is doing
	// exactly what I wanted to do, but with a plus: in that source they speak about
	// the escape of resources (Yes, it is another query, I know,  but it is useful,
	// (remember Michel is a Colombian searching in baidu and he probably has no
	// idea about Mandarin ;) ).
	_, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		file, err := os.Create(filepath)
		if err != nil {
			// the boss told me : Michel if something in the module fails you should use os.Exit1, you don't have to handle the error, the order is os.Exit1
			fmt.Fprintf(os.Stderr, "createFile failed creating the file %v\n", err)
			os.Exit(1)
		}

		err = file.Close()
		if err != nil {
			// the boss told me : Michel if something in the module fails you should use os.Exit1, you don't have to handle the error, the order is os.Exit1
			fmt.Fprintf(os.Stderr, "createFile failed closing the file %v\n", err)
			os.Exit(1)
		}

		// Phase 3: this is like magic for Michel: ok ok chinese source showed me
		// what I wished for but chinese source was not making return of io.Writer,
		// honestly to write this line I took a deep breath and said I will trust
		// golang magic.
		// I was already smelling the interfaces for some strange reason.
		// and ......
		// BOOM ! : the magic exploded for me and with it a hundred questions.
		// Magic , the divine fire of interfaces in GOLANG ???
		// I think : yes because Duck typing is about:
		// 		If it walks like a duck and it quacks like a duck, then
		// 		it must be a duck,
		// In one post of Dave Cheney I found this:
		// 		https://research.swtch.com/interfaces
		// I am finding the power of Golang?? , I want to believe : Yes.
		// interfaces here is the explanation for me: THIS Method is
		// equal to the line named // SIMILAR ? in this SAME module, but here
		// Golang is returning a io.Writer not a File, review the source:
		// https://golang.org/src/os/file.go?s=9055:9092#L300
		return os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0644)
		// I must confess my life was one before this line and then my life was
		// another after this line (My life as a Gopher :) )
		// I had ideas like: If the power of Golang interfaces is that great, I
		// think I can rewrite a good part of the core that is python in Golang
		// and improve speed. Maybe I'll start working on it without my boss
		// knowing about it(I love Python but I hadn't felt that magical feeling
		// at work in a long time.).
	}

	return nil, ErrFileExists
}

// FUnction where err = file.Close() really function
func writeToFile(filepath, strtowrite string) error {
	// line named SIMILAR ?
	file, err := os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("writeToFile failed with error1:", err)

		return fmt.Errorf("writeToFile failed with error1: %w", err)
	}

	// but here I am using the same thing os.OpenFile
	err = file.Close()
	if err != nil {
		fmt.Println("writeToFile failed with error2:", err)
		os.Exit(1)
	}

	if _, err := file.WriteString(strtowrite); err != nil {
		fmt.Println("writeToFile failed with error3:", err)

		return fmt.Errorf("writeToFile failed with error3: %w", err)
	}

	return nil
}

// Phase 4 : Investigate again :
// My question about wasting resources kept floating in my mind, hitting hard.
// So I had to think for myself and I connected these ideas:
//  	1. io.Writer does not have a Close() method or function or implementation
//  	(FIle yes BUT IS NOT THE CASE Michel).
// 		2. Is io.Copy who finally uses the interface to complete the writing
//		job.
//      3. Does io.Copy have something to close the resource or file it uses?
// What happens with the topic: am I leaking Sources ?.
// I have been looking and reading code in a lot of sources:
/*

https://golang.org/src/io/io.go?s=13362:13422#L402

https://golang.org/src/os/file.go?s=4749:4806#L174

https://golang.org/src/os/file_plan9.go#L24

https://github.com/golang/go/blob/3cb64ea39e0d71fe2af554cbf4e99d14bc08d41b/src/syscall/syscall_linux_amd64.go

But I can not to solve the question by myself : What happens with the topic: am I leaking Sources ?.
Golang Garbage Collector solve the problem? What i don't know ?

Here In the code I am using any close Method and the unique thing here
is the unique reset in the process is in
Buffer at:
	https://golang.org/src/bytes/buffer.go?s=7711:7769#L255

But After is hour of the io.Writter again without a Close() method ....

https://gitlab.com/deBmichel/study-book-thegoprogramminglanguage/-/raw/9cb8a0f4f13ed182ab68e8c6377c41a5d710ad1b/reprsrcs/gotools/alternativeextractor.go
https://gitlab.com/deBmichel/study-book-thegoprogramminglanguage/-/raw/3e50df502a18a0a98562e168b4ff9b3e89ae8b31/reprsrcs/gotools/codeextractor.go
*/
