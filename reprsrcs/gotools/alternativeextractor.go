/*
	Title?:
	An unexpected encounter with the power of Golang interfaces.
	CLEAN SOLUTION

	I was using this solution to read the book and go downloading
	the code of the Alan A. A. Donovan open repo.

*/
package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
)

var ErrFileExists = errors.New("the file really exists")

func main() {
	//if writer, err := createFile(os.Args[2]); err == nil { // createFile(io.Writer /*The magic is here???*/, error)
	if writer, err := createFileAlt(os.Args[2]); err == nil { // no Magic here (*os.File
		url := os.Args[1]
		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}

		// os.File can be used in the io.Copy and I can close the file
		// But I have the question what happens with the interface ??
		// if I use interface is there a tactic to call the close method
		// using reflect ? If yes : Is a great practice is a great idea.
		_, err = io.Copy(writer, resp.Body)
		errcloseBody := resp.Body.Close()
		if errcloseBody != nil {
			fmt.Fprintf(os.Stderr, "fetch: closing Body 1%s: %v\n", url, err)
			os.Exit(1)
		}

		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: writting 1%s: %v\n", url, err)
			os.Exit(1)
		}

		endstr := "\n// I (Michel MS) found this code in the source :\n// " + os.Args[1] + "\n"
		_, err = io.Copy(writer, bytes.NewBufferString(endstr))
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: writting 2%s: %v\n", url, err)
			os.Exit(1)
		}

		// Solving leaking sources?. using io.File can close the file
		err = writer.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: closing Writer 1%s: %v\n", url, err)
			os.Exit(1)
		} else {
			fmt.Println("Really closed the file ")
		}

	} else {
		fmt.Fprintf(os.Stderr, "CreateFile has failed with error: %v\n", err)
	}
}

func createFile(filepath string) (io.Writer /*The magic is here???*/, error) {
	_, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		file, err := os.Create(filepath)
		if err != nil {
			// the boss told me : Michel if something in the module fails you should use os.Exit1, you don't have to handle the error, the order is os.Exit1
			fmt.Fprintf(os.Stderr, "createFile failed creating the file %v\n", err)
			os.Exit(1)
		}

		err = file.Close()
		if err != nil {
			// the boss told me : Michel if something in the module fails you should use os.Exit1, you don't have to handle the error, the order is os.Exit1
			fmt.Fprintf(os.Stderr, "createFile failed closing the file %v\n", err)
			os.Exit(1)
		}

		return os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0644)
	}

	return nil, ErrFileExists
}

func createFileAlt(filepath string) (*os.File /*NO Magic here*/, error) {
	_, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		file, err := os.Create(filepath)
		if err != nil {
			// the boss told me : Michel if something in the module fails you should use os.Exit1, you don't have to handle the error, the order is os.Exit1
			fmt.Fprintf(os.Stderr, "createFile failed creating the file %v\n", err)
			os.Exit(1)
		}

		err = file.Close()
		if err != nil {
			// the boss told me : Michel if something in the module fails you should use os.Exit1, you don't have to handle the error, the order is os.Exit1
			fmt.Fprintf(os.Stderr, "createFile failed closing the file %v\n", err)
			os.Exit(1)
		}
		return os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0644)
	}

	return nil, ErrFileExists
}
