/*
	The purgatory requirement said core functions should be pure and
	said that core functions should be pure and that repeating code in
	core function triggers or callers was not acceptable.

	The only way to do that in Golang in an acceptable way is with
	interceptors(Middlewares) and isolating the callers to the functions.
*/

package iter

import (
	"errors"
	"reflect"
	"time"
)

var ErrNotArrayOrSlice = errors.New("arg is not array or slice")

func mddlwrVerifyListSlice(iterCoreFunc func([]interface{}) time.Duration) func(interface{}) {
	return func(arg interface{}) {
		listinterface, islistinterfaceerr := verifySliceOrArrayList(arg)
		if islistinterfaceerr == nil {
			iterCoreFunc(listinterface)
		} else {
			Logger().LogError("mddlwrVerifyListSlice found error:", islistinterfaceerr)
		}
	}
}

func verifySliceOrArrayList(arg interface{}) (ret []interface{}, err error) {
	theKind := reflect.TypeOf(arg).Kind()
	validkinds := [2]reflect.Kind{
		reflect.Array, reflect.Slice,
	}

	for i := 0; i < len(validkinds); i++ {
		if theKind == validkinds[i] {
			toconvert := reflect.ValueOf(arg)
			lenconvert := toconvert.Len()
			converted := make([]interface{}, lenconvert)

			/*
				One Lesson: converting a []string to an []interface{} is O(n)
				time because each element of the slice must be converted to an
				interface{}: review the code in this function I am paying a
				high price in computational time in this function, I did not
				decide this, this is something that arises from complying with
				the requirement.
					@(Me)DeBMichel.
			*/

			for i := 0; i < lenconvert; i++ {
				converted[i] = toconvert.Index(i).Interface()
			}

			return converted, nil
		}
	}

	Logger().LogError("verifyListSlice found bad Kind:", theKind, "value:", arg)

	return ret, ErrNotArrayOrSlice
}
