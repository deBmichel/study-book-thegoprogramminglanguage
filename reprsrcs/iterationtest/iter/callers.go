package iter

func ExecuteIterateWithRange(arg interface{}) {
	mddlwrVerifyListSlice(iterateWithRange)(arg)
}

// Iterate and iterate and iterate
func ExecuteIterateWithFor(arg interface{}) {
	mddlwrVerifyListSlice(iterateWithFor)(arg)
}

func ExecuteIterateWithRecursiveTactical(arg interface{}) {
	mddlwrVerifyListSlice(iterateWithRecursiveTactical)(arg)
}
