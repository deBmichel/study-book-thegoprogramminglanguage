/*
	About this module:

	The purgatory requirement said promptly: a slice of whatever in the core
	functions and a slice of whatever in golang is []interface{} I am secure.
	@(me)DeBMichel

	Functions are three, and all functions should return an aprox of time
	execution.
*/

package iter

import (
	"reflect"
	"time"
)

func iterateWithRange(arg []interface{}) time.Duration {
	startAt := time.Now()

	for _, val := range arg {
		reflect.TypeOf(val).Kind()
	}

	return time.Since(startAt)
}

func iterateWithFor(arg []interface{}) time.Duration {
	startAt := time.Now()
	indx, indxlimit := 0, len(arg)

	for ; indx < indxlimit-1; indx++ {
		reflect.TypeOf(arg[indx]).Kind()
	}

	return time.Since(startAt)
}

func iterateRecursive(arg []interface{}, indx int) {
	if indx < len(arg)-1 {
		reflect.TypeOf(arg[indx]).Kind()
		iterateRecursive(arg, indx+1)
	}
}

func iterateWithRecursiveTactical(arg []interface{}) time.Duration {
	startAt := time.Now()

	iterateRecursive(arg, 0)

	return time.Since(startAt)
}
