/*
	Making benchmarks private avoids the testing driver trying to
	invoke it directly, which would fail as its signature does not
	match: I found this in:
		https://dave.cheney.net/2013/06/30/how-to-write-benchmarks-in-go
		@DmitrG shared this with me.

	From the source I found this:
		As the testing package uses a simple average (total time to run
		the benchmark function over b.N) this result is statistically weak.
		You can increase the minimum benchmark time using the -benchtime
		flag to produce a more accurate result.

		to run an example maybe this, without forget the -benchtime flag
		looking for produce a more accurate result.

		go test -bench=TesterExecuteIterateWithRangeSlice -benchtime=20s
*/

package iter_test

import (
	"reflect"
	"testing"
	"time"

	iter "tecnilacchallenge/iter"
)

func Thefunctstotest() []func() func(arg []interface{}) time.Duration {
	return []func() func(arg []interface{}) time.Duration{
		iter.TesterIterateWithRange,
		iter.TesterIterateWithFor,
		iter.TesterIterateWithRecursiveTactical,
	}
}

func benchmarkExecuteIterateWithRange(b *testing.B, testarg []interface{}) {
	// Linting review found test helper function should start from b.Helper() (thelper)
	// I had to review this source https://github.com/kulti/thelper and learn about:
	// With this call go test prints correct lines of code for failed tests. Otherwise,
	// printed lines will be inside helpers functions.
	b.Helper()

	var (
		// Forcer is a minor try to avoid compiling optimization 3:)
		forcer time.Duration
		r      time.Duration
	)

	for i := 0; i < b.N; i++ {
		r = Thefunctstotest()[0]()(testarg)
	}

	forcer = r
	// A minor func to use the forcer and comply with the basic rules
	// of Golang: If you define a variable it is to use it
	reflect.TypeOf(forcer)
}

func BenchmarkTesterExecuteIterateWithRangeSlice1(b *testing.B) {
	benchmarkExecuteIterateWithRange(b, SliceA())
}

func BenchmarkTesterExecuteIterateWithRangeSlice2(b *testing.B) {
	benchmarkExecuteIterateWithRange(b, SliceB())
}

func benchmarkTesterIterateWithRecursiveTactical(b *testing.B, testarg []interface{}) {
	b.Helper()

	var (
		forcer time.Duration
		r      time.Duration
	)

	for i := 0; i < b.N; i++ {
		r = Thefunctstotest()[2]()(testarg)
	}

	forcer = r
	reflect.TypeOf(forcer)
}

func BenchmarkTesterIterateWithRecursiveTacticalSlice1(b *testing.B) {
	benchmarkTesterIterateWithRecursiveTactical(b, SliceA())
}

func BenchmarkTesterIterateWithRecursiveTacticalSlice2(b *testing.B) {
	benchmarkTesterIterateWithRecursiveTactical(b, SliceB())
}

func benchmarkTesterIterateWithFor(b *testing.B, testarg []interface{}) {
	b.Helper()

	var (
		forcer time.Duration
		r      time.Duration
	)

	for i := 0; i < b.N; i++ {
		r = Thefunctstotest()[1]()(testarg)
	}

	forcer = r
	reflect.TypeOf(forcer)
}

func BenchmarkTesterIterateWithForSlice1(b *testing.B) {
	benchmarkTesterIterateWithFor(b, SliceA())
}

func BenchmarkTesterIterateWithForSlice2(b *testing.B) {
	benchmarkTesterIterateWithFor(b, SliceB())
}
