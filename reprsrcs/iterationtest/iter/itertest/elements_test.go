// Fprint formats using the default formats for its operands and writes to w.
// Spaces are added between operands when neither is a string.
// It returns the number of bytes written and any write error encountered.

package iter_test

import (
	"math/cmplx"
)

const (
	C1 = iota
	C2
	C4 = 99
)

// SliceA returns a []interface{} with a all datatypes to test iter package
func SliceA() []interface{} {
	// Beautifull lesson := byte('ф') constant 1092 overflows byte
	return []interface{}{
		'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж',
		byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'),
		'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж',
		byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'),
		'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж', 'ж',
		byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'), byte('s'),
		"gбвгджзклмнпрстфхцчшщ123汉字汉字汉字汉字汉字汉字999999999999asdf", "999999999999asdfg123汉字汉字汉字汉字汉字汉字",
		"gбвгджзклмнпрстфхцчшщ123汉字汉字汉字汉字汉字汉字999999999999asdf", "999999999999asdfg123汉字汉字汉字汉字汉字汉字",
		"gбвгджзклмнпрстфхцчшщ123汉字汉字汉字汉字汉字汉字999999999999asdf", "999999999999asdfg123汉字汉字汉字汉字汉字汉字",
		"gбвгджзклмнпрстфхцчшщ123汉字汉字汉字汉字汉字汉字999999999999asdf", "999999999999asdfg123汉字汉字汉字汉字汉字汉字",
		true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false,
		true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false,
		true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false,
		true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false,
		true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false,
		true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false,
		99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999,
		99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999,
		99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999,
		99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999,
		99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999, 99999999999999999,
		int(C4), int8(C4), int16(C4), int32(C4), int64(C4), uint(C4), uint8(C4), uint16(C4), uint32(C4), uint64(C4),
		uintptr(C4), int(C4), int8(C4), int16(C4), int32(C4), int64(C4), uint(C4), uint8(C4), uint16(C4), uint32(C4),
		uint64(C4), uintptr(C4), int(C4), int8(C4), int16(C4), int32(C4), int64(C4), uint(C4), uint8(C4), uint16(C4),
		uint32(C4), uint64(C4), uintptr(C4), int(C4), int8(C4), int16(C4), int32(C4), int64(C4), uint(C4), uint8(C4),
		uint16(C4), uint32(C4), uint64(C4), uintptr(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4),
		float64(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4),
		float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4),
		float64(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4),
		float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4), float32(C4), float64(C4),
		cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i),
		cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i),
		cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i),
		cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i),
		cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i), cmplx.Sqrt(-5 + 12i),
	}
}

func SliceB() []interface{} {
	base, ret := SliceA(), []interface{}{}

	for i := 0; i < 7; i++ {
		for j := 0; j < len(base); j++ {
			ret = append(ret, base[j])
		}
	}

	return ret
}
