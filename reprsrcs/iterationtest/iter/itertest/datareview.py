import matplotlib.pyplot as plt

ns = [2**x for x in range(10)];
# data from go test -bench=. -benchtime=20s  
data = {
  'for': [2762, 2753, 2756, 2763, 2766, 18447, 18543, 18562, 18593, 18602],
  'forrange': [2927, 2942, 2948, 2958, 2961, 19422, 19456, 19496, 19508, 19580],
  'recursion': [4167, 4201, 4262, 4248, 4256, 35514, 35584, 35650, 35703, 35730],
}

plt.yscale('linear')
#plt.yscale('log')
for (label, values) in data.items():
    plt.plot(ns, values, label=label)


plt.legend()
plt.show()





