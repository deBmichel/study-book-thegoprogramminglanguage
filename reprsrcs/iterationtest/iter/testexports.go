/*
	This module is a bridge to Test private functions in the package:

	@Me(DeBMichel) : I have never been on the test team and I have not touched
	the files with suffix: _test.

	export_test.go only be include when we run go test, so it not pollute your
	API, and user never access them(not like java’s @VisibleForTesting), It
	build a bridge let unexported one accessible. @Me(DeBMichel) took this from
	  : https://medium.com/@robiplus/golang-trick-export-for-test-aa16cbd7b8cd

	To complete the challenge I asked for help to someone in the test team and
	@TGNatyk taught me:
	I have been elite in test team but I was software developer and as you I
	love break my mind with code: about test files near source code you don't
	have why to be scared:
	This logic and what is written here is not mixed with the source code;
	But rest assured, all our review metrics are deeper than the North
	American ones and the western ones and we have our own framework for it.
	It would be serious if in your branches there were findings of calls of
	test functions from the source code.

	Here we use Exports but we Dont like global vars, remember that: test code
	have to pass the same linters than source code project ; and as you I am not
	happy with global vars finding dear; Go is not about objects or states, it
	is secuential communications , the concept of actor is very different to
	objects. I will give a gift Michel: The files with suffix _test to implement
	testing exports are not a great idea and you will find the error undefined.
	Try renaming this module to testexports_test.go and execute a test.
*/

package iter

import (
	"time"
)

func TesterIterateWithRange() func(arg []interface{}) time.Duration {
	return iterateWithRange
}

func TesterIterateWithFor() func(arg []interface{}) time.Duration {
	return iterateWithFor
}

func TesterIterateWithRecursiveTactical() func(arg []interface{}) time.Duration {
	return iterateWithRecursiveTactical
}
