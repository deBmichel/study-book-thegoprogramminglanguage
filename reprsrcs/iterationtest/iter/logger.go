package iter

import (
	"log"
	"os"
	"sync"
)

const iterfilelog = "iterlogs.log"

type TheIterLogger struct {
	warningLogger *log.Logger
	infoLogger    *log.Logger
	errorLogger   *log.Logger
}

func (l *TheIterLogger) StartTheIterLogger() {
	log.Println("Preparing logger instance")

	file, err := os.OpenFile(iterfilelog, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	// Be quiet houze with the file log, if the file doesn't exist create it.

	l.infoLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	l.warningLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	l.errorLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func (l *TheIterLogger) LogWarning(args ...interface{}) {
	l.warningLogger.Println(args...)
}

func (l *TheIterLogger) LogInfo(args ...interface{}) {
	l.infoLogger.Println(args...)
}

func (l *TheIterLogger) LogError(args ...interface{}) {
	l.errorLogger.Println(args...)
}

var (
	ErrPttrnImsingletoninstance *TheIterLogger
	ErrPttrnImsingletononcer    sync.Once
)

func Logger() *TheIterLogger {
	if ErrPttrnImsingletoninstance == nil {
		ErrPttrnImsingletononcer.Do(func() {
			ErrPttrnImsingletoninstance = &TheIterLogger{}
			ErrPttrnImsingletoninstance.StartTheIterLogger()
		})
	}

	return ErrPttrnImsingletoninstance
}
