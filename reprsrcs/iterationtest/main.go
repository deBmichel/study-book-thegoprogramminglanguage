package main

import (
	printer "log"

	iter "tecnilacchallenge/iter/itertest"
)

func main() {
	printer.Println("Executing iter package:")

	funcs := iter.Thefuncts()
	for f := 0; f < len(funcs); f++ {
		printer.Println(".................................... Calling function ....................................")

	}

	printer.Println("Finished execution review the logs")
}
