<div align="center">
	Michel MS Personal Study of the book:
	<h1> The Go Programming Language </h1>
	<img src="reprsrcs/imagebook.png" alt="Knightgif" width="126px" height="166px"/>
</div>

It is a repo created with the intention of making a personal study of the 
book The Go Programming Language by the authors:

    Alan A. Donovan Google Inc 
    Brian W. Kernighan.

This book is a referent book for people or programmers or software developers 
who like or want to learn about the Golang programming language.

<h1>
	Piracy is not allowed in this repository so you won't find an illegal
	<br> 
	copy of this great book here
</h1>

<h1>
You can get a legal copy of this great book reviewing at:<br /><br />
https://www.amazon.com/Programming-Language-Addison-Wesley-Professional-Computing/dp/0134190440<br/><br/>
https://www.gopl.io/<br/><br/>
</h1>

All code used in this repo was taken from :

<h1>https://github.com/adonovan/gopl.io</h1>

Programs in https://github.com/adonovan/gopl.io are licensed under a Creative Commons 
Attribution-NonCommercial-ShareAlike 4.0 International License.

For this study I was using golang programming languaje with versions:

- go1.14 linux/amd64
- go1.16 linux/amd64

