package main

import "fmt"

type Point struct {
	x, y int
}

func main() {
	var p Point
	p.x, p.y = 1, 2
	fmt.Println(p)
}
