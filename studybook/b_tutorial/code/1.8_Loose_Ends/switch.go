/*
	I wrote this code by myself to play with switch 3:)
*/
package main

import (
	"os"
	"strconv"
)

func main() {
	println("Playing with normal switch")
	for i := 1; i < len(os.Args); i++ {
		n, _ := strconv.Atoi(os.Args[i])
		switch {
		case n == 1:
			println("You have 1")
		case n == 2:
			println("You have 2")
		case n == 3:
			println("You have 3")
		case n == 4:
			println("You have 4")
		case n < 2:
			println("You have MINOR THAN CERO")
		default:
			println("You have nothing to 1 2 3 4")
		}
	}

	println("Playing with tagless switch 1 .......")
	for i := 1; i < len(os.Args); i++ {
		n, _ := strconv.Atoi(os.Args[i])
		switch {
		default:
			println("You have nothing to 1 2 3 4")
		case n <= 2:
			println("You have MINOR THAN CERO")
		case n == 1:
			println("You have 1")
		case n == 2:
			println("You have 2")
		case n == 3:
			println("You have 3")
		case n == 4:
			println("You have 4")
		}
	}

	println("Playing with tagless switch 2 .......")
	for i := 1; i < len(os.Args); i++ {
		n, err := strconv.Atoi(os.Args[i])
		if err != nil {
			println("here", n, "cause")
		}
		switch true {
		case n <= 2:
			println("You have MINOR THAN CERO")
		case n == 1:
			println("You have 1")
		case n == 2:
			println("You have 2")
		default:
			println("You have nothing to 1 2 3 4")
		case n == 3:
			println("You have 3")
		case n == 4:
			println("You have 4")
		}
	}
}
