/*
	Modify fetch to also print the HTTP status code, found in resp.Status.

	Note : To solve this exercise I used the code of my solution to the exercise
	two in this section.
*/

package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func main() {
	for _, url := range os.Args[1:] {
		if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
			fmt.Println("Adding prefix http:// to the source", url)
			url = "http://" + url
			fmt.Println("now, with prefix url is :", url)
		}

		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}

		fmt.Println("Answer status code to", url, ":\n\t", resp.Status, "\nAnswer Body:\n\t")
		/*
			Some lessons:
				resp.Body type is io.Reader
				io.Copy src should be io.Reader

			Question:
				what about a defer against a for ? :) in this case about
				defer resp.Body.Close() 3:)
		*/

		_, err = io.Copy(os.Stdout, resp.Body)
		resp.Body.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: reading %s: %v\n", url, err)
			os.Exit(1)
		}
	}
}
