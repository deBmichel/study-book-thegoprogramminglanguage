// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See page 1.

// Helloworld is our first Go program.
//!+
package main

import "fmt"

func main() {
	fmt.Println("Hello, 世界")
}

//!-

// I (Michel MS)found this code in the source:
// https://github.com/adonovan/gopl.io/blob/master/ch1/helloworld/main.go
