// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See page 4.
//!+

// Echo1 prints its command-line arguments.
package main

import (
	"fmt"
	"os"
)

func main() {
	var s, sep string
	for i := 1; i < len(os.Args); i++ {
		s += sep + os.Args[i]
		sep = " " // smells like magic here ?? the problem was : my linters break the code in two lines cause ; is an interesting idea.
	}
	fmt.Println(s)
}

//!-

// I (Michel MS)found this code in the source:
// https://github.com/adonovan/gopl.io/blob/master/ch1/echo1/main.go
