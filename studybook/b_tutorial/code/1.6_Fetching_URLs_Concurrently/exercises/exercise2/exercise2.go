/*exercise2.go
Try fetchall with longer argument lists, such as samples from
the top million web sites available at alexa.com . How does
the program behave if a web site just doesn’t respond?
(Section 8.9 describes mechanisms for coping in such cases.)

To solve this exercise I used the code of the exercise 1 in this repo.
*/
package main

import (
	"errors"
	prntr "fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

var ErrFileExists = errors.New("the file really exists")

func main() {
	start := time.Now()
	filepath := strings.Split(start.String(), ".")[0]

	if err := createFile(filepath); err == nil {
		ch := make(chan string)

		for _, url := range os.Args[1:] {
			if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
				url = "https://" + url
			}

			go fetch(url, ch) // start a goroutine
		}

		for range os.Args[1:] {
			_ = writeToFile(filepath, <-ch+"\n")
		}

		finaltime := prntr.Sprintf("%.2fs elapsed\n", time.Since(start).Seconds())
		_ = writeToFile(filepath, finaltime)
	} else {
		prntr.Println("CreateFile has failed with error:", err)
	}
}

func fetch(url string, ch chan<- string) {
	start := time.Now()

	resp, err := http.Get(url)
	if err != nil {
		ch <- prntr.Sprintf("Bad url get request %s error = { %v }", url, err) // send to channel ch

		return
	}

	nbytes, err := io.Copy(ioutil.Discard, resp.Body)
	resp.Body.Close() // don't leak resources

	if err != nil {
		ch <- prntr.Sprintf("while reading %s: %v", url, err)

		return
	}

	secs := time.Since(start).Seconds()
	ch <- prntr.Sprintf("Good url get request %s %.2fs  %7d", url, secs, nbytes)
}

func createFile(filepath string) error {
	_, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		file, err := os.Create(filepath)
		if err != nil {
			prntr.Println("File creation failed with error", err)

			return prntr.Errorf("file creation failed with error: %w", err)
		}

		file.Close()

		return nil
	}

	return ErrFileExists
}

func writeToFile(filepath, strtowrite string) error {
	file, err := os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		prntr.Println("writeToFile failed with error1:", err)

		return prntr.Errorf("writeToFile failed with error1: %w", err)
	}

	defer file.Close()

	if _, err := file.WriteString(strtowrite); err != nil {
		prntr.Println("writeToFile failed with error2:", err)

		return prntr.Errorf("writeToFile failed with error2: %w", err)
	}

	return nil
}
