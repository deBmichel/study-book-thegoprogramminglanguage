// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See page 10.
//!+

// Dup2 prints the count and text of lines that appear more than once
// in the input.  It reads from stdin or from a list of named files.
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int)
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts) // os.Stdin = os.Open ? = *os.File
	} else {
		for _, arg := range files {
			f, err := os.Open(arg) // os.Open = os.Stdin ? = *os.File
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, counts)
			f.Close()
		}
	}
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}
}

func countLines(f *os.File, counts map[string]int) {
	// Reference default ?? with receive a map ??
	/*
		When a map is passed to a function, the function receives a
		copy of the reference, so any changes the calle d function
		makes to the underlying data structure will be visible through
		the caller’s map reference too.
	*/
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
	}
	// NOTE: ignoring potential errors from input.Err()
}

//!-

// I (Michel MS)found this code in the source(I had to do some changes to test
// the code by myself :) ):
// https://github.com/adonovan/gopl.io/blob/master/ch1/dup2/main.go
