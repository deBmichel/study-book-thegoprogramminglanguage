/*
	Exercise:
		Modify dup2 to print the names of all files in which each duplicated
		line occurs.
*/

// Dup2 prints the count and text of lines that appear more than once
// in the input.  It reads from stdin or from a list of named files.

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]map[string]int)
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, counts)
			f.Close()
		}
	}
	for file, count := range counts {
		for line, n := range count {
			if n > 1 {
				fmt.Printf("%s\t%d\t%s\n", file, n, line)
			}
		}
	}
}

func countLines(f *os.File, counts map[string]map[string]int) {
	input := bufio.NewScanner(f)
	if counts[f.Name()] == nil {
		// solves error : panic: assignment to entry in nil map
		counts[f.Name()] = make(map[string]int)
		// if counts[f.Name()] == nil { avoid repeat the count sure,
		for input.Scan() {
			counts[f.Name()][input.Text()]++
		}
	} else {
		// important verify and review the execution of all the code: SURE
		fmt.Println("File with name : ", f.Name(), "; really was counted")
	}
	// NOTE: ignoring potential errors from input.Err()
}

//!-

// I (Michel MS)found this code in the source(I had to do some changes to test
// the code by myself :) ):
// https://github.com/adonovan/gopl.io/blob/master/ch1/dup2/main.go
