// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See page 8.
//!+

// Dup1 prints the text of each line that appears more than
// once in the standard input, preceded by its count.
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int)
	fmt.Println(counts)
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		counts[input.Text()]++ // this line is equal to : line = input.Test(); counts[line] = counts[line]++
	}
	fmt.Println(counts)
	// NOTE: ignoring potential errors from input.Err()
	for line, n := range counts {
		if n > 1 { // I had to change the code to test the code and understand the context ....
			fmt.Printf("%d\t%s\n", n, line)
		} // Use a else is a great idea , is sure .. CONTEXT : DUPLICATES .....
	}
}

//!-

// I (Michel MS)found this code in the source:
// https://github.com/adonovan/gopl.io/blob/master/ch1/dup1/main.go.
// (But is obvious I made some changes to test the code :) ).
