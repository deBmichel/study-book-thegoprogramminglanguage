/*
	Here I used the base of the authors but I made some modifications
	I wished print something in the backend and use io.Copy to change
	fmt.

	@author: Michel MS
*/
package main

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", handler) // each request calls handler
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	message := "URL.Path = " + r.URL.Path + "\n"
	_, err := io.Copy(w, bytes.NewBufferString(message))
	if err != nil {
		log.Println(os.Stderr, "Failed in io.copy %v\n", err)
		os.Exit(1)
	}

	_, _ = io.Copy(os.Stdout, bytes.NewBufferString("found"+message))
}
