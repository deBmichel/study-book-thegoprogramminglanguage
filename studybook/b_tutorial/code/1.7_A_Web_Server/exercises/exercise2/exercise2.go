/*
	Modify the Lissajous server to read parameter values from the URL.
	For example, you might arrange it so that a URL like :
	http://localhost:8000/?cycles=20 sets the number of cycles to 20
	instead of the default 5. Use the strconv.Atoi function to convert
	the string parameter into an integer. You can see its documentation
	with go doc strconv.Atoi.

	In this exercise I used as base code the code from the exercise1.
*/

package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

func main() {
	http.HandleFunc("/", handler) // each request calls handler
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	log.Println("Receive request at", time.Now().String())

	if err := r.ParseForm(); err != nil {
		log.Print(err)
	}

	for k, v := range r.Form {
		if k == "cycles" {
			thecycles, err := strconv.Atoi(v[0])
			if err != nil {
				fmt.Fprintf(w, "You are not giving a numeric value to the cycles\n")

				return
			}
			lissajous(w, thecycles)
		}
	}

	fmt.Fprintf(w, "You are not giving the cycles, be carefull cowboy\n")
}

func lissajous(out io.Writer, thecycles int) {
	palette := []color.Color{color.White, color.Black}
	cycles := float64(thecycles)
	const (
		whiteIndex = 0     // first color in palette
		blackIndex = 1     // next color in palette
		res        = 0.001 // angular resolution
		size       = 100   // image canvas covers [-size..+size]
		nframes    = 64    // number of animation frames
		delay      = 8     // delay between frames in 10ms units
	)
	freq := rand.Float64() * 3.0 // relative frequency of y oscillator
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase difference
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5),
				blackIndex)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim) // NOTE: ignoring encoding errors
}
